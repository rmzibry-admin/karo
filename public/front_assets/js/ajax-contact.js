$(function() {

	// Get the form.
	var form = $('#app-form');

	// Get the messages div.
	var formMessages = $('.form-message');

	// Set up an event listener for the contact form.
	$(form).submit(function(e) {
		// Stop the browser from submitting the form.
		e.preventDefault();

		// Serialize the form data.
		var formData = $(form).serialize();

		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response) {

            if(response.error == 0){
                $('#success-msg').show();
                $('#error-msg').hide();
            }

            //verify-form
            if(response.data && response.available == 1){
                $('#invalid-error-msg').hide();
                $.each(response.data, function (k, v) {
                    $("#" + k).text(v);
                })
            }else if(response.available == 0){
                $('.verify').text('');
                $('#invalid-error-msg').show();
            }


			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('error');
			$(formMessages).addClass('success');

			// Set the message text.
			$(formMessages).text(response);

			// Clear the form.
			$('#contact-form input,#contact-form textarea').val('');
		})
		.fail(function(data) {

			// Make sure that the formMessages div has the 'error' class.
			$(formMessages).removeClass('success');
			$(formMessages).addClass('error');

			// Set the message text.
			if (data.responseText !== '') {
				$(formMessages).text(data.responseText);
                if(data.responseJSON.errors){
                    $('#error-msg').show();
                    $('#success-msg').hide();
                }
			} else {
				$(formMessages).text('Oops! An error occured and your message could not be sent.');
			}
		});
	});

    $('#reset-btn').on("click", function(){
        $('#fill-required').hide();
        $('#success-msg').hide();
     });

});

<?php
namespace App\Library;

use Validator;
use Auth;

class Login
{
    public function credentials($req,$remember){
        // validating.
        $validate = $this->validate($req,$remember);
        if(!$validate){
            return response([
                "error" => 1,
                "msg" => "Please enter your credentials"
            ]);
        }else{
            return $validate;
        }
    }

    private function validate($req,$remember){
        $validator = Validator::make($req,[
            "username" => "required",
            "password" => "required"
        ]);
//        echo Hash::make($req['password']);die;
        if($validator->fails()){
            return false;
        }else{
            if(Auth::attempt([
                "name" => $req["username"],
                "password" => $req["password"],
            ],$remember)){
                $error = 0;
                $msg = "";
            }else{
                $error = 2;
                $msg = "The credentials you have entered don't match";
            }

            return response([
                "error" => $error,
                "msg" => $msg
            ]);
        }
    }
}
?>

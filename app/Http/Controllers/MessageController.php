<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use App\Message;


class MessageController extends Controller
{
    public function table()
    {
        Message::query()->update(['flag' => 1]);

        return view('backend.message');
    }

    public function dataTable(){

        $data = DB::table('messages')->get();
        return Datatables::of($data)->make(true);
    }

    public function deleteRow(Request $req){
        $id = $req->id;

        $delete = DB::table("messages")->where('id',$id)->delete();
        if ($delete) {
            return response([
                'delete' => 1
            ]);
        }
    }
}

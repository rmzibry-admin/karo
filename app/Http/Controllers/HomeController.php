<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Message;

class HomeController extends Controller
{
    public function index(){

        return view('front.home');
    }

    public function aboutus(){

        return view('front.about');
    }

    public function contactus(){

        return view('front.contact');
    }

    public function verification(){

        return view('front.verification');
    }

    public function faculties(){

        return view('front.the-faculties');
    }

    public function recognition(){

        return view('front.recognitions');
    }

    public function admission(){

        return view('front.admissions');
    }

    public function entryRequirements(){

        return view('front.entry-requirements');
    }

    public function downloadView(){

        return view('front.download');
    }


    public function download(){

        $files = DB::table('pdf_files')->get();

        return view('frontend.download',compact('files'));
    }

    public function onlineapplication(){

        return view('front.onlineapplication');
    }

    public function refundPolicy(){

        return view('front.refund-policy');
    }

    public function sendMessage(Request $req){
        $req->validate([
            'fname' => 'required',
            'email'=> 'required',
            'subject'=> 'required',
            'phone'=> 'required:digits:15',
        ]);

        //$date = date("l jS \of F Y h:i:s A");
        $date = date("d/m/Y");

        $add = Message::UpdateOrCreate([
            'fname' => "$req->fname",
            'lname' => $req->lname ?? ' ',
            'email' => "$req->email",
            'subject' => "$req->subject",
            'message' => "$req->message",
            'date' => "$date",
            'phone' => "$req->phone",
        ]);

        if($add){
            return response([
                'status' => 1,
                'message' => 'Message Sent Succesfully..!'
            ]);
        }
    }

    public function faculty($faculty = null)
    {
        if($faculty == "art"){
            return view('front.faculties.art');
        }elseif($faculty == "business"){
            return view('front.faculties.bussiness');
        }elseif($faculty == "engineering"){
            return view('front.faculties.engineering');
        }elseif($faculty == "islamic"){
            return view('front.faculties.islamic');
        }elseif($faculty == "language"){
            return view('front.faculties.language');
        }elseif($faculty == "law"){
            return view('front.faculties.law');
        }elseif($faculty == "medicine"){
            return view('front.faculties.medicine');
        }elseif($faculty == "science"){
            return view('front.faculties.science');
        }else{
            return view('front.the-faculties');
        }
        return view('front.the-faculties');
    }
}

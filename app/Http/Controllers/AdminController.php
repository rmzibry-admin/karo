<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use App\Verification;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Application;
use App\Message;

class AdminController extends Controller
{
    public function dashboard()
    {
        $data['new_applications'] = Application::where('flag',0)->count();
        $data['new_verifications'] = 0;
        $data['new_messages'] = Message::where('flag',0)->count();

        $data['applications'] = Application::count();
        $data['verifications'] = Verification::count();
        $data['messages'] = Message::count();

        return view('backend.dashboard',$data);
    }

    public function verification()
    {
        $allCountries = Verification::allCountries();

        return view('backend.verification',compact('allCountries'));
    }

    public function editVerification($id)
    {
        $allCountries = Verification::allCountries();
        $data = Verification::find($id);

        return view('backend.edit-verification',compact('allCountries','data'));
    }

    public function getDownload($id)
    {
        $ver =  Verification::find($id);
        $pdf = $ver->pdf;
        //PDF file is stored under project/public/download/info.pdf
        $file = public_path(). "/pdfs".'/'.$pdf;
        if(File::exists($file)){

            $headers = array(
            'Content-Type: application/pdf',
            );

            return Response::download($file, $pdf, $headers);
        }

        return 'Completion Letter not found..!';
    }

    public function addVerification(Request $req)
    {
        $msg = "";
        $error = "";

        $validate = validator::make($req->all(), [
            'f_name' => 'required',
            'l_name' => 'required',
            'course' => 'required',
            // 'learner_no' => 'required|Unique:verifications',
            'certificate_no' => 'required|Unique:verifications',
            'country' => 'required',
            'pdf' => 'required',
            'image' => 'required',
            'completion_date' => 'required'
        ], [
            "f_name.required" => "The first name field is required.",
            "l_name.required" => "The last name field is required.",
            'learner_no.unique' => 'The Student Id has already been taken.',
        ]);
        if ($validate->fails()) {
            $error = 1;
            $msg = $validate->messages();
        } else {

            $input = $req->all();

            $input['image'] = time().'.'.$req->file('image')->extension();
            $req->image->move(public_path('images/verifications'), $input['image']);

            $input['pdf'] = time().'.'.$req->file('pdf')->extension();
            $req->pdf->move(public_path('pdfs'), $input['pdf']);

            $add = Verification::add($input);
            if ($add) {
                $error = 0;
            } else {
                $error = 1;
                $msg = "something went wrong";
            }
        }

        return response([
            "error" => $error,
            "msg" => $msg
        ]);
    }

    public function updateVerification(Request $req,$id)
    {
        $msg = "";
        $error = "";

        $this->validate($req, [
            'f_name' => 'required',
            'l_name' => 'required',
            'course' => 'required',
            //'learner_no' => 'required|Unique:verifications',
            //'certificate_no' => 'required|Unique:verifications',
            'country' => 'required',
            //'pdf' => 'required',
            //'image' => 'required',
            'completion_date' => 'required'
        ], [
            "f_name.required" => "The first name field is required.",
            "l_name.required" => "The last name field is required.",
            'learner_no.unique' => 'The Student Id has already been taken.',
        ]);

        // if ($validate->fails()) {
        //     $error = 1;
        //     $msg = $validate->messages();
        // } else {

            $input = $req->all();

            $update =  Verification::find($id);
            $input['certificate_no'] = $update->certificate_no;

            if($req->file('image')){
                $input['image'] = time().'.'.$req->file('image')->extension();
                $req->image->move(public_path('images/verifications'), $input['image']);
                @unlink(public_path('images/verifications/').$update->image);
            }else{
                $input['image'] = $update->image;
            }

            if ($req->file('pdf')) {
                $input['pdf'] = time().'.'.$req->file('pdf')->extension();
                $req->pdf->move(public_path('pdfs/'), $input['pdf']);
                @unlink(public_path('pdfs').$update->pdf);
            }else{
                $input['pdf'] = $update->pdf;
            }

            $add = Verification::change($input);
            if (!$add) {
                return redirect()->back()->with('error', 'Something went wrong!');
            }
        //}

        return redirect()->back()->with('success', 'Successfully updated!');
    }

    public function verificationTable()
    {

        $data = Verification::orderBy('id','DESC')->get();

        return Datatables::of($data)->make(true);
    }

    public function deleteVerification(Request $req)
    {
        $id = $req->id;

        $delete = Verification::find($id)->delete();
        if ($delete) {
            return response([
                'error' => 0
            ]);
        }
    }

    public function verify(Request $req)
    {
        $req->validate([
            'certNo' => 'required',
        ]);

        $data = Verification::where("certificate_no", $req->certNo)->first();

        if (empty($data)) {
            $available = 0;
            $data = null;
        } else {
            $available = 1;
        }

        return response([
            "available" => $available,
            "data" => $data,
        ]);
    }

    public function changeSetting(Request $req)
    {
        $password = $req->current_pwd;
        $id = $req->id;
        $username = $req->username;
        $new_password = $req->new_pwd;
        $confirm_password = $req->con_pwd;
        $error = 0;
        $msg = "";

        if (empty($password)) {
            $msg = "Enter your current password";
            $error = 1;
        } else {
            $validate = $this->validatePass($id, $password);
            if (!$validate) {
                $error = 1;
                $msg = "Your password is wrong";
            } else if ($new_password !== $confirm_password) {
                $error = 1;
                $msg = "Password doesn't match";
            } else if (empty($username) && empty($new_password)) {
                $error = 1;
                $msg = "No changes were made";
            } else if (!empty($username) && !empty($new_password)) {
//            change username and password
                $update = DB::table('users')->where('id',$id)->update([
                    'name' => $username,
                    'password' => Hash::make($new_password),
                ]);
                if($update) $error = 0;
            } else {
                if (!empty($username)) {
//                chnage username
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $username
                    ]);
                    if($update) $error = 0;
                }

                if (!empty($new_password)) {
//               change password
                    $update = DB::table('users')->where('id',$id)->update([
                        'password' => Hash::make($new_password)
                    ]);
                    if($update) $error = 0;
                }
            }
        }


        return response([
            'error' => $error,
            'msg' => $msg
        ]);
    }

    private function validatePass($id, $password)
    {
        $match = false;
        $user = DB::table('users')->where('id', $id)->first();
        if (Hash::check($password, $user->password)) {
            $match = true;
        }

        return $match;
    }

    public function form(){

        return view('backend.files');
    }

    public function uploadFile(Request $req){
        $error = 0;
        $msg = "";

        $validate = Validator::make($req->all(),[
            'file' => 'required|mimes:pdf',
            'name' => 'required|unique:pdf_files'
        ],[
            'file.required' => 'Upload a pdf file.',
            'file.mimes' => 'This file must be a pdf.'
        ]);

        if($validate->fails()){
            $error = 1;
            $msg = $validate->messages();
        }else{
            $file = $req->file;
            $name = $req->name;
            $extension = $file->getClientOriginalExtension();
            $filename = $name.'.'.$extension;
            $destination = 'upload/';
            $file->move($destination,$filename);

            $add = DB::table('pdf_files')->insert([
                'name' => $name
            ]);

            if($add){
                $error = 0;
            }
        }

        return response([
            'error' => $error,
            'msg' => $msg
        ]);
    }

    public function filesTable(Request $req){
        $data = DB::table("pdf_files")->get();
        return Datatables::of($data)->make(true);
    }

    public function deleteFiles(Request $req){
        $id = $req->id;

        $data = DB::table('pdf_files')->find($id);
        $name = $data->name.".pdf";
        $destination = 'upload';
        $remove = File::delete($destination."/".$name);
        if($remove){
            $delete = DB::table("pdf_files")->where('id',$id)->delete();
            if($delete){
                return response([
                    'delete' => 1
                ]);
            }
        }
    }
}

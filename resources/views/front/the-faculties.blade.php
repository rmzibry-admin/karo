@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(f-assets/images/banner/banner3.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculties</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Faculties</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-8 col-sm-12">
                            <h2 class="title-head">Our <span>Faculties</span></h2>
                            <p>Karo University offers the kind of education needed for leadership in a rapidly changing
                                world.</p>
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic1.jpg') }}" alt="">
                                            <a href="{{ route('faculty','art') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','art') }}">Arts & Humanities</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic2.jpg') }}" alt="">
                                            <a href="{{ route('faculty','business') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','business') }}">Business & Social Sciences</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic3.jpg') }}" alt="">
                                            <a href="{{ route('faculty','language') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','language') }}">Language & Cultural</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic4.jpg') }}" alt="">
                                            <a href="{{ route('faculty','medicine') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','medicine') }}">Medicine & Health</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic5.jpg') }}" alt="">
                                            <a href="{{ route('faculty','engineering') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','engineering') }}">Engineering</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic6.jpg') }}" alt="">
                                            <a href="{{ route('faculty','science') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','science') }}">Science & Technology</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic7.jpg') }}" alt="">
                                            <a href="{{ route('faculty','law') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','law') }}">Law</a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="{{ asset('f-assets/images/courses/pic8.jpg') }}" alt="">
                                            <a href="{{ route('faculty','islamic') }}" class="btn">Courses</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <span>Faculty of</span>
                                            <h5><a href="{{ route('faculty','islamic') }}">Islamic Education</a></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection

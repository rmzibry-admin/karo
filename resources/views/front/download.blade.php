@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(f-assets/images/banner/banner3.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Downloads</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Downloads</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <!--div class="col-lg-3 col-md-4 col-sm-12 m-b30">
           <div class="course-detail-bx">
            <div class="course-buy-now text-center">
             <a href="#" class="btn radius-xl text-uppercase">Apply Online</a>
            </div>
           </div>
          </div -->
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="m-b30" id="curriculum">
                                <h4>Downloads</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>For Students</span>
                                                    <h5><a href="#">Student Application</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>Faculty Deens</span>
                                                    <h5><a href="#">Management System Document</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>Lecturers</span>
                                                    <h5><a href="#">Course Guide</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>Student Union</span>
                                                    <h5><a href="#">Student Union Application</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>Department Deens</span>
                                                    <h5><a href="#">Department Management Documents</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>For Students</span>
                                                    <h5><a href="#">Union Election Guidelines</a></h5>
                                                </div>
                                                <span>
                                                    <div class="course-buy-now text-center">
                                                        <a href="#" class="btn radius-xl text-uppercase">Download</a>
                                                    </div>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
@endsection

@extends('front.layouts.app')

@section('content')
    <!-- Inner Content Box ==== -->
    <div class="page-content">
        <!-- Page Heading Box ==== -->
        <div class="page-banner ovbl-dark" style="background-image:url(f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">About Us</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>About Us</li>
                </ul>
            </div>
        </div>
        <!-- Page Heading Box END ==== -->
        <!-- Page Content Box ==== -->
        <div class="content-block">
            <!-- Our Story ==== -->
            <div class="section-area bg-gray section-sp1 our-story">
                <div class="container">
                    <div class="row align-items-center d-flex">
                        <div class="col-lg-5 col-md-12 heading-bx">
                            <h2 class="m-b10">Our Story</h2>
                            <h5 class="fw4">About Karo University (University Karo)</h5>
                            <p>Universitas Karo (Karo University) is a private higher-education school in Karo, North
                                Sumatra, that was founded in 1986. Karo is a medium-sized city with a population of
                                250,000-499,999 people. Karo University has been established with the objective of making a
                                noteworthy contribution to the social, economic and cultural life of our country. Having
                                faith in the power of education, the builders of this university intend to impart knowledge
                                to youngsters of society.</p>
                        </div>
                        <div class="col-lg-7 col-md-12 heading-bx p-lr">
                            <div class="video-bx">
                                <img src="{{ asset('f-assets/images/about/pic1.jpg') }}" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Our Story END ==== -->
            <!-- About Content ==== -->
            <div class="section-area section-sp2 bg-fix ovbl-dark join-bx text-center"
                style="background-image:url(f-assets/images/background/bg1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="join-content-bx text-white">
                                <h4>Vision</h4>
                                <p>Karo University will distinguish itself as an open-access university by building
                                    innovative academic courses and programs responsive to student, community, and market
                                    needs. Created to enable professional competence, these academic offerings will be
                                    available in face-to-face and/or varied online formats so that distance, time and cost
                                    will not be barriers to learning or student-centered service. We will foster an
                                    inclusive environment where diversity is valued and understanding and respect for others
                                    is the norm.</p>
                                <h4>Mission</h4>
                                <p>Karo University is a great idea, great works, great conversations, distance learning
                                    university that offers graduate education in the humanities with concentrations in
                                    imaginative literature, natural science, philosophy and religion, and social science.
                                    Karo University promotes student-faculty scholarship through research, discussion, and
                                    the development of collaborative publications.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Content END ==== -->
            <!-- Our Status ==== -->
            <div class="section-area content-inner section-sp1">
                <div class="container">
                    <div class="section-content">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-primary">
                                        <span class="counter">90</span><span>+</span>
                                    </div>
                                    <span class="counter-text">Total Courses</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-black">
                                        <span class="counter">5</span><span>M</span>
                                    </div>
                                    <span class="counter-text">Total Students</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-primary">
                                        <span class="counter">2800</span><span>+</span>
                                    </div>
                                    <span class="counter-text">Questions Answered</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-black">
                                        <span class="counter">10.2</span><span>K</span>
                                    </div>
                                    <span class="counter-text">Total Books</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Our Status END ==== -->
        </div>
        <!-- Page Content Box END ==== -->
    </div>
    <!-- Inner Content Box END ==== -->
@endsection

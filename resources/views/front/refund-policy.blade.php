@extends('front.layouts.app')

@section('content')
    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8"
        style="background-image: url({{ url('front_assets/images/page-banner-5.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>Refund Policy</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admission') }}">Admissions</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Refund Policy</li>
                            </ol>
                        </nav>
                    </div> <!-- page banner cont -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->

    <!--====== ENTRY REQ START ======-->

    <section id="event-singel" class="pt-30 pb-0 gray-bg">
        <div class="container">
            <div class="events-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="events-left"></div>

                        <p class="font-weight-bold">Students who find it necessary to withdraw from the program must submit
                            a written notice to the University. </p>
                        <br>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">After completing more than 10% of the total
                                module assignments and up to and including completion of 25% of the assignments, 25% of the
                                refundable tuition.</li>
                            <li class="list-group-item list-group-item-action">After completing more than 25% of the total
                                module assignments and up to and including completion of 50% of the assignments, 10% of the
                                refundable tuition.</li>
                            <li class="list-group-item list-group-item-action">If the student completes more than half of
                                the total module assignments or the enrollment period has expired, the school will be
                                entitled to the full tuition and no refund will be able. The amount of the module completed
                                will be the ratio of completed assignments received by the University to the total
                                assignments required to complete the module.</li>
                        </ul>

                    </div> <!-- events left -->
                </div>

            </div> <!-- events right -->
        </div>
        </div> <!-- row -->
        </div> <!-- events-area -->
        </div> <!-- container -->
    </section>

    <!--====== ENTRY REQ PART ENDS ======-->

@endsection

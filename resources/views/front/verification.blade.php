@extends('front.layouts.app')
@push('styles')
    <style>
        .popup .content {
            top: 60%;
        }

        @media only screen and (max-device-width: 480px) {
            .popup.active {
                z-index: 100000;
                position: relative;
            }
        }

    </style>
@endpush
@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Verification</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- Main Slider -->
        <div class="section-area section-sp1 bg-fix online-cours">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center text-white text-blue">
                        <h2>Online Certificate Verification</h2>
                        <form id="add-verification-form" class="cours-search">
                            <h6>Please Enter your certificate verification number</h6>
                            <div class="input-group">
                                <input id="certNo" type="text" class="form-control"
                                    placeholder="Example: KU/CC312/CI022352" required name="certNo">
                                <button class="btn" type="submit">Verify
                                    Now</button>
                            </div>
                        </form>
                        <div class="text-error">
                            <h6 id="errormsg"></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Slider -->
        <!-- inner page banner END -->
        <div class="popup" id="popup-1">
            <div class="overlay"></div>
            <div class="content">
                <div class="close-btn" onclick="togglePopupClose()">&times;</div>
                <h3 class="popuphead">VERIFICATION STATUS</h3>
                <!-- inner page banner -->
                <div id="printcont">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="text-white contact-info-bx">
                                    <img class="bg-border" src="{{ asset('f-assets/images/profile/pic1.jpg') }}" id="image">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12 col-12 m-b30">
                                <form class="contact-bx ajax-form">
                                    <div class="ajax-message"></div>
                                    <div class="row placeani">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>FULL NAME</span>
                                                    <h6>
                                                        <span id="f_name"></span>
                                                        <span id="l_name"></span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>PROGRAMME</span>
                                                    <h6 id="course"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>REGISTER NUMBER</span>
                                                    <h6 id="learner_no"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>COURSE</span>
                                                    <h6 id="course"></h6>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>GPA</span>
                                                    <h6 id="gpa"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>COMPLETION DATE</span>
                                                    <h6 id="completion_date"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>METHOD OF STUDY</span>
                                                    <h6 id="mode"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span>COUNTRY</span>
                                                    <h6 id="country">Sri Lanka</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 mb-3">
                                            <a type="submit" id="pdf-download" class="btn button-md pricomlet">Completion
                                                Letter</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- inner page banner END -->
            </div>

        </div>
        <!-- Content END-->
    </div>
@endsection
@push('scripts')
    <!--====== jquery js ======-->
    <script type="text/javascript">
        //     $('#form').on('submit', function(e) {
        //     e.preventDefault();

        //     let certNo = $('#certNo').val();

        //     $.ajax({
        //         url: $(form).attr('action'),
        //         type: "POST",
        //         data: {
        //             "_token": "{{ csrf_token() }}",
        //             certNo: certNo
        //         },
        //         success: function(response) {
        //             $('#successMsg').show();
        //             console.log(response);
        //         },
        //         error: function(response) {
        //             $('#nameErrorMsg').text(response.responseJSON.errors.name);
        //             $('#emailErrorMsg').text(response.responseJSON.errors.email);
        //             $('#mobileErrorMsg').text(response.responseJSON.errors.mobile);
        //             $('#messageErrorMsg').text(response.responseJSON.errors.message);
        //         },
        //     });
        // });

        $("#add-verification-form").submit(function(e) {
            e.preventDefault();
            var btn = $("#submit-btn");
            btn.text("Loading..");
            var url = $base_url + '/verify';
            var type = "post";
            var data = $(this).serialize();
            ajPost(url, data, type, function(r) {
                if (r.available == 0) {
                    $("#errormsg").fadeIn().text("Invalid Certificate Number, Please check!");
                } else {
                    $("#errormsg").fadeOut();
                    $.each(r.data, function(k, v) {
                        $("#" + k).text(v);
                    })

                    if (r.data.id && r.data.pdf) {
                        var pdfDownloadUrl = "{{ url('download') }}/" + r.data.id;
                        $('#pdf-download').attr('href', pdfDownloadUrl);
                    }

                    if (r.data.image) {
                        var imageUrl = "{{ asset('/images/verifications') }}/" + r.data.image;
                       if(imageUrl) $('#image').attr('src', imageUrl);
                    }

                    //

                    $('#popup-1').addClass("active");
                }

                btn.text("Search");
            })
        })

        function printDiv(divName) {
            $('.pricomlet').hide();
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
            $('.pricomlet').show();
        }

        // const myform = document.getElementById("myform");
        // myform.addEventListener("submit", (e) => {
        //     e.preventDefault();
        // });

        function togglePopup(idname) {
            var value = document.getElementById('certvlu').value;
            if (value.length > 15) {
                document.getElementById(idname).classList.toggle("active");
            } else {
                document.getElementById("errormsg").innerHTML = "Invalid Certificate Number, Please check!";
            }
        }

        function togglePopupClose() {
            $('#popup-1').removeClass("active");
        }
    </script>
@endpush

@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(f-assets/images/banner/banner3.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Contact Us</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->

        <!-- inner page banner -->
        <div class="page-banner contact-page section-sp2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-5 m-b30">
                        <div class="bg-primary text-white contact-info-bx">
                            <h2 class="m-b10 title-head">Contact <span>Information</span></h2>
                            <p>Monday 7:30AM – 4:30PM <br> Tuesday 7:30AM – 4:30PM <br> Wednesday 7:30AM – 4:30PM <br>
                                Thursday 7:30AM – 4:30PM <br> Friday 7:30AM – 4:30PM <br> Saturday Closed <br> Sunday Closed
                            </p>
                            <div class="widget widget_getintuch">
                                <ul>
                                    <li><i class="ti-location-pin"></i>JL. Letjen Djamin Ginting, 22151, Titi Rantai,
                                        Kabanjahe, Kota Medan, Sumatera Utara 20157, Indonesia</li>
                                    <li><i class="ti-mobile"></i>+62 628 20248 (8AM - 4PM Support Line)</li>
                                    <li><i class="ti-email"></i>info@unikaro.id</li>
                                </ul>
                            </div>
                            <h5 class="m-t0 m-b20">Follow Us</h5>
                            <ul class="list-inline contact-social-bx">
                                <li><a href="#" class="btn outline radius-xl"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="btn outline radius-xl"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="btn outline radius-xl"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="btn outline radius-xl"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <form class="contact-bx ajax-form" id="app-form" action="{{ route('store.message') }}" method="post">
                            <input type="hidden" name="lname" value=" ">
                            <input type="hidden" name="date" value=" ">
                        
                            <div class="heading-bx left">
                                <h2 class="title-head">Get In <span>Touch</span></h2>
                                <p>Feel free to contact us if you need any assistance, any help or another question.</p>
                            </div>
                            <div class="row placeani">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Your Name</label>
                                            <input name="fname" type="text" required class="form-control valid-character">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Your Email Address</label>
                                            <input name="email" type="email" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Your Phone</label>
                                            <input name="phone" type="text" required class="form-control int-value">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Subject</label>
                                            <input name="subject" type="text" required class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Type Message</label>
                                            <textarea name="message" rows="4" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="g-recaptcha"
                                                data-sitekey="6Lf2gYwUAAAAAJLxwnZTvpJqbYFWqVyzE-8BWhVe"
                                                data-callback="verifyRecaptchaCallback"
                                                data-expired-callback="expiredRecaptchaCallback"></div>
                                            <input class="form-control d-none" style="display:none;" data-recaptcha="true"
                                                required data-error="Please complete the Captcha">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-lg-12">
                                    <div class="ajax-message"></div>
                                    <button name="submit" type="submit" value="Submit" class="btn button-md"> Send
                                        Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
    </div>
    <!-- Content END-->
@endsection

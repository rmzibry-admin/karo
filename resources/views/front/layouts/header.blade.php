@if (!Request::is('admin'))
    @if (Request::is('/'))
        <header class="header rs-nav header-transparent">
            <div class="top-bar">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="topbar-left">
                            <ul>
                                <li><a href="{{ route('contact') }}"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
                                <li><a href="mailto:info@unikaro.id"><i class="fa fa-envelope-o"></i>info@unikaro.id</a></li>
                            </ul>
                        </div>
                        <div class="topbar-right">
                            <ul>
                                <li><a href="{{ route('admin') }}">Login</a></li>
                                <li><a href="{{ route('verification') }}">Verification</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sticky-header navbar-expand-lg">
                <div class="menu-bar clearfix">
                    <div class="container clearfix">
                        <!-- Header Logo ==== -->
                        <div class="menu-logo">
                            <a href="{{ route('home') }}"><img src="{{ asset('f-assets/images/logo-white.png') }}" alt=""></a>
                        </div>
                        <!-- Mobile Nav Button ==== -->
                        <button class="navbar-toggler collapsed menuicon justify-content-end" type="button"
                            data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <!-- Author Nav ==== -->
                        <div class="secondary-menu">
                            <div class="secondary-inner">
                                <ul>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <!-- Search Button ==== -->
                                    <li class="search-btn"><button id="quik-search-btn" type="button"
                                            class="btn-link"><i class="fa fa-search"></i></button></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Search Box ==== -->
                        <div class="nav-search-bar">
                            <form action="#">
                                <input name="search" value="" type="text" class="form-control"
                                    placeholder="Type to search">
                                <span><i class="ti-search"></i></span>
                            </form>
                            <span id="search-remove"><i class="ti-close"></i></span>
                        </div>
                        <!-- Navigation Menu ==== -->
                        <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                            <div class="menu-logo">
                                <a href="{{ route('home') }}"><img src="{{ asset('f-assets/images/logo.png') }}" alt=""></a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{ route('home') }}">Home</a></li>
                                <li class="active"><a href="{{ route('about') }}">About</a></li>
                                <li class="active"><a href="{{ route('faculties') }}">Faculties</a></li>
                                <li class="active"><a href="{{ route('recognition') }}">Recognitions</a></li>
                                <li class="active"><a href="{{ route('download') }}">Downloads</a></li>
                                <li class="active"><a href="{{ route('contact') }}">Contact Us</a></li>
                            </ul>
                            <div class="nav-social-link">
                                <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                                <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                                <a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <!-- Navigation Menu END ==== -->
                    </div>
                </div>
            </div>
        </header>
    @else
        <header class="header rs-nav">
            <div class="top-bar">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="topbar-left">
                            <ul>
                                <li><a href="{{ route('contact') }}"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
                                <li><a href="mailto:info@unikaro.id"><i class="fa fa-envelope-o"></i>info@unikaro.id</a>
                                </li>
                            </ul>
                        </div>
                        <div class="topbar-right">
                            <ul>
                                <li><a href="{{ route('admin') }}">Login</a></li>
                                <li><a href="{{ route('verification') }}">Verification</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sticky-header navbar-expand-lg">
                <div class="menu-bar clearfix">
                    <div class="container clearfix">
                        <!-- Header Logo ==== -->
                        <div class="menu-logo">
                            <a href="index.html"><img src="{{ asset('f-assets/images/logo.png') }}" alt=""></a>
                        </div>
                        <!-- Mobile Nav Button ==== -->
                        <button class="navbar-toggler collapsed menuicon justify-content-end" type="button"
                            data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <!-- Author Nav ==== -->
                        <div class="secondary-menu">
                            <div class="secondary-inner">
                                <ul>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li><a href="javascript:;" class="btn-link"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <!-- Search Button ==== -->
                                    <li class="search-btn"><button id="quik-search-btn" type="button"
                                            class="btn-link"><i class="fa fa-search"></i></button></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Search Box ==== -->
                        <div class="nav-search-bar">
                            <form action="#">
                                <input name="search" value="" type="text" class="form-control"
                                    placeholder="Type to search">
                                <span><i class="ti-search"></i></span>
                            </form>
                            <span id="search-remove"><i class="ti-close"></i></span>
                        </div>
                        <!-- Navigation Menu ==== -->
                        <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                            <div class="menu-logo">
                                <a href="index.html"><img src="{{ asset('f-assets/images/logo.png') }}" alt=""></a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{ route('home') }}">Home</a></li>
                                <li class="active"><a href="{{ route('about') }}">About</a></li>
                                <li class="active"><a href="{{ route('faculties') }}">Faculties</a></li>
                                <li class="active"><a href="{{ route('recognition') }}">Recognitions</a></li>
                                <li class="active"><a href="{{ route('download') }}">Downloads</a></li>
                                <li class="active"><a href="{{ route('contact') }}">Contact Us</a></li>
                            </ul>
                            <div class="nav-social-link">
                                <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                                <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                                <a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <!-- Navigation Menu END ==== -->
                    </div>
                </div>
            </div>
        </header>
    @endif
@endif

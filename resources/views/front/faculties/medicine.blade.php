@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(../f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculty of Medicine & Health</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('faculties') }}">Faculties</a></li>
                    <li>Faculty of Medicine & Health</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                            <div class="course-detail-bx">
                                <div class="course-buy-now text-center">
                                    <a href="{{ route('application') }}" class="btn radius-xl text-uppercase">Apply Online</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div class="courses-post">
                                <div class="ttr-post-info">
                                    <div class="ttr-post-title ">
                                        <h2 class="post-title">Faculty of Medicine & Health</h2>
                                    </div>
                                    <div class="ttr-post-text">
                                        <p>The Faculty of Medicine and Health is committed to leading this change, by
                                            educating and shaping future healthcare professionals and reimagining the way
                                            healthcare is delivered.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="m-b30" id="curriculum">
                                <h4>Courses</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>1.</span> Anesthesia
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>2.</span> Anatomy
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>3.</span> Biomedical Science
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>4.</span> Dentistry
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>5.</span> Dermatology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>6.</span> Medicine / Surgery
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>7.</span> Natural / Alternative Medicine
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>8.</span> Nursing
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>9.</span> Obstetrics / Gynecology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>10.</span> Optometry / Ophthalmology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>11.</span> Orthopedics
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>12.</span> Otorhinolaryngology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>13.</span> Pediatrics
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>14.</span> Pathology Studies
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>15.</span> Podiatry
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>16.</span> Psychiatry
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>17.</span> Radiography
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>18.</span> Speech / Rehabilitation / Physiotherapy
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>19.</span> Other Medical & Health Studies
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection

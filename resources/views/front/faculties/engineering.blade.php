@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(../f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculty of Engineering</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('faculties') }}">Faculties</a></li>
                    <li>Faculty of Engineering</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                            <div class="course-detail-bx">
                                <div class="course-buy-now text-center">
                                    <a href="{{ route('application') }}" class="btn radius-xl text-uppercase">Apply Online</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div class="courses-post">
                                <div class="ttr-post-info">
                                    <div class="ttr-post-title ">
                                        <h2 class="post-title">Faculty of Engineering</h2>
                                    </div>
                                    <div class="ttr-post-text">
                                        <p>Engineers possess a rare combination – a seamless blend of the practical and the
                                            creative. It’s a profession that tackles society’s challenges and makes the
                                            world a better place to live. And what better place to study engineering than
                                            the best faculty for engineering in Indonesia - that’s us.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="m-b30" id="curriculum">
                                <h4>Courses</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>1.</span> Aeronautical Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>2.</span> Agricultural Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>3.</span> Architectural Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>4.</span> Biomedical Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>5.</span> Chemical Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>6.</span> Civil and Environmental Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>7.</span> Computer and IT Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>8.</span> Electronic and Electrical Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>9.</span> General Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>10.</span> Geological Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>11.</span> Industrial Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>12.</span> Mechanical / Manufacturing Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>13.</span> Mining and Metallurgical Engineering
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>14.</span> Other Engineering Studies
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection

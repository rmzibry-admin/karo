@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(../f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculty of Arts & Humanities</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('faculties') }}">Faculties</a></li>
                    <li>Faculty of Arts & Humanities</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                            <div class="course-detail-bx">
                                <div class="course-buy-now text-center">
                                    <a href="{{ route('application') }}" class="btn radius-xl text-uppercase">Apply Online</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div class="courses-post">
                                <div class="ttr-post-info">
                                    <div class="ttr-post-title ">
                                        <h2 class="post-title">Faculty of Arts & Humanities</h2>
                                    </div>
                                    <div class="ttr-post-text">
                                        <p>The Faculty of Arts and Humanities is well known nationally and internationally.
                                            It offers high quality education at undergraduate and postgraduate levels in a
                                            variety of fields, and has a substantial research profile.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="m-b30" id="curriculum">
                                <h4>Courses</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>1.</span> Applied Arts
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>2.</span> Classics
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>3.</span> Design
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>4.</span> Education
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>5.</span> Fine Arts
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>6.</span> History
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>7.</span> Literature
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>8.</span> Museum Studies
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>9.</span> Performing Arts
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>10.</span> Philosophy
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>11.</span> Religion and Theology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>12.</span> Visual Arts
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>13.</span> Other Arts & Humanities Studies
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection

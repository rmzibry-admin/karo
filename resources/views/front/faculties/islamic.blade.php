@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(../f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculty of Islamic Education</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('faculties') }}">Faculties</a></li>
                    <li>Faculty of Islamic Education</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                            <div class="course-detail-bx">
                                <div class="course-buy-now text-center">
                                    <a href="{{ route('application') }}" class="btn radius-xl text-uppercase">Apply Online</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div class="courses-post">
                                <div class="ttr-post-info">
                                    <div class="ttr-post-title ">
                                        <h2 class="post-title">Faculty of Islamic Education</h2>
                                    </div>
                                    <div class="ttr-post-text">
                                        <p>The Faculty of Islamic Education provides the students with a good sound of
                                            knowledge in the field of Islamic studies. It offers a wide range of courses on
                                            the subject matters of Arabic Language, Quran, Sunnah and Shariah.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="m-b30" id="curriculum">
                                <h4>Courses</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>1.</span> Islamic Studies
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection

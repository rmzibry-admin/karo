@extends('front.layouts.app')

@section('content')
    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8"
        style="background-image: url({{ url('front_assets/images/page-banner-5.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>Entry Requirements</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admission') }}">Admissions</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Entry Requirements</li>
                            </ol>
                        </nav>
                    </div> <!-- page banner cont -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->

    <!--====== ENTRY REQ START ======-->

    <section id="event-singel" class="pt-30 pb-0 gray-bg">
        <div class="container">
            <div class="events-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="events-left"></div>

                        <p class="font-weight-bold">Candidates for Admission to all graduate programs must:</p>
                        <br>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">Submit a 1-2 page typed narrative outlining
                                one’s intention <br> (e.g., reasons for undertaking a college degree, professional
                                aspirations, etc.).</li>
                            <li class="list-group-item list-group-item-action">Duly signed and fully completed the
                                Application Form for Admissions</li>
                            <li class="list-group-item list-group-item-action">Copies of all educational Certificates</li>
                            <li class="list-group-item list-group-item-action">A Details CV (Bio Data)</li>
                            <li class="list-group-item list-group-item-action">A Passport Size Color Photos</li>
                            <li class="list-group-item list-group-item-action">Copies of Working Experience Letters</li>
                            <li class="list-group-item list-group-item-action">Passport Detail Page Copy</li>
                            <li class="list-group-item list-group-item-action">A Confirmation Letter from the Approved
                                Center</li>
                            <li class="list-group-item list-group-item-action">Two Academic Reference Letters</li>
                        </ul>

                    </div> <!-- events left -->
                </div>

            </div> <!-- events right -->
        </div>
        </div> <!-- row -->
        </div> <!-- events-area -->
        </div> <!-- container -->
    </section>

    <!--====== ENTRY REQ PART ENDS ======-->

@endsection

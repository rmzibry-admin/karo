<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('admin/dashboard')) active @endif">
        <a class="nav-link" href="{{url('/admin/dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        User Interface
    </div>

    <li class="nav-item @if(request()->is('admin/verification')) active @endif">
        <a class="nav-link" href="{{url('/admin/verification')}}">
            <i class="fas fa-certificate"></i>
            <span>Verifications</span></a>
    </li>

    <li class="nav-item @if(request()->is('admin/messages')) active @endif">
        <a class="nav-link" href="{{url('/admin/messages')}}">
            <i class="fa fa-mail-bulk"></i>
            <span>Messages</span></a>
    </li>

    {{-- <li class="nav-item @if(request()->is('admin/files')) active @endif">
        <a class="nav-link" href="{{url('/admin/files')}}">
            <i class="fa fa-file-pdf"></i>
            <span>Files</span></a>
    </li> --}}

    <li class="nav-item @if(request()->is('admin/application')) active @endif">
        <a class="nav-link" href="{{url('/admin/application')}}">
            <i class="fa fa-newspaper"></i>
            <span>Applications</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>

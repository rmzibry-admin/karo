<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">@yield('heading')</h1>
        @yield('add-new-btn')
    </div>

    @yield('content')


{{--    admin settings--}}
    <div class="modal fade bd-example-modal-lg" id="setting-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                Add new information
                            </div>

                            <div class="col-md-2">
                                <p id="setting-success-msg" class="badge text-success" style="font-size: 14px"></p>
                                <p id="error-msg" class="badge text-danger" style="font-size: 14px"></p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="setting-form">
                            <div class="form-group row">
                                <label class="col-md-3">Current Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="current_pwd" value="">
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label class="col-md-3">Username</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control setting-inp" name="username" value="">
                                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">New Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control setting-inp" name="new_pwd" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Confirm password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control setting-inp" name="con_pwd" value="">
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group text-right">
                            <button id="setting-btn" type="submit" class="btn btn-outline-success btn-sm">Change</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

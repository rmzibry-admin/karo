<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>

{{-- sweetalert --}}
<script src="{{ asset('assets/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.all.min.js') }}"></script>

{{-- datepicker --}}
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Page level plugins -->
{{-- <script src="{{asset('assets/vendor/chart.js/Chart.min.js')}}"></script> --}}

<!-- Page level custom scripts -->
{{-- <script src="{{asset('assets/js/demo/chart-area-demo.js')}}"></script> --}}
{{-- <script src="{{asset('assets/js/demo/chart-pie-demo.js')}}"></script> --}}

{{-- datatable --}}
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
    });
    var $base_url = '{{ Request::url() }}';
</script>
<script src="{{ asset('assets/js/myScript.js?141') }}"></script>
<script src="{{ asset('assets/js/loaddatatable.js?2') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#datepicker-year').datepicker({
        minViewMode: 2,
        format: 'yyyy'
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $('#drop').select2();
    $('#country').select2();
</script>

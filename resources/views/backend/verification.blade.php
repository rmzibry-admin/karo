@extends('backend.layouts.master')

@section('heading')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">

@endsection

@section('add-new-btn')
    <button class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#add-new-verification-modal">
        <i class="fa fa-plus"></i>
        Add New
    </button>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Certificate verification
                </div>
                <div class="card-body table-responsive">
                    <table id="verification-table" class="table table-borderless">
                        <thead>
                            <th> Firstname</th>
                            <th> Lastname</th>
                            <th> Student ID</th>
                            <th> Certificate No</th>
                            <th> Course</th>
                            <th> GPA</th>
                            <th> Country</th>
                            <th> Letter</th>
                            <th> Image</th>
                            <th> Action</th>
                        </thead>
                        <tbody class="text-capitalize">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- add new model --}}
    <div class="modal fade bd-example-modal-lg" id="add-new-verification-modal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                Add new information
                            </div>

                            <div class="col-md-2">
                                {{-- <p id="success-msg" class="badge text-success" style="font-size: 14px"></p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="add-verification-form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group row">
                                <label class="col-md-3">Learner Number</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="certificate_no">
                                    <label class="badge text-danger error-badge" id="certificate_no"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">First name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="f_name">
                                    <label class="badge text-danger error-badge" id="f_name"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Last name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="l_name">
                                    <label class="badge text-danger error-badge" id="l_name"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Programme</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="course">
                                    <label class="badge text-danger error-badge" id="course"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">GPA</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="gpa" step=".01">
                                    <label class="badge text-danger error-badge" id="gpa"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Completion Date</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="completion_date" >
                                    <label class="badge text-danger error-badge" id="completion_date"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Method of study</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="mode" >
                                    <label class="badge text-danger error-badge" id="mode"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Country</label>
                                <div class="col-md-6">
                                    <div class="form-control form-group pt-0">
                                        <select id="country" class="input-field" style="width: 100%" name="country"
                                            data-error="Country is required." required="required">
                                            <option value="">Select Country..</option>
                                            @foreach ($allCountries as $county)
                                                <option value="{{ $county }}"> {{ $county }}</option>
                                            @endforeach
                                        </select>
                                        <label class="badge text-danger error-badge" id="country"></label>
                                    </div> <!-- singel form -->
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">Completion Letter</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="pdf" accept="application/pdf">
                                    <label class="badge text-danger error-badge" id="pdf"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Profile Photo</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="image" accept="image/*" >
                                    <label class="badge text-danger error-badge" id="image"></label>
                                </div>
                            </div>

                            {{-- <div class="form-group row">
                                <label class="col-md-3">Course</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="course">
                                    <label class="badge text-danger error-badge" id="course"></label>
                                </div>
                            </div> --}}

                           
                            {{-- <div class="form-group row">
                                <label class="col-md-3">Student ID</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="learner_no">
                                    <label class="badge text-danger error-badge" id="learner_no"></label>
                                </div>
                            </div> --}}

                    </div>
                    <div class="card-footer">
                        <div class="form-group text-right">
                            <p id="success-msg" class="badge text-success" style="font-size: 14px"></p>
                            <button id="submit-btn" type="submit" class="btn btn-outline-success btn-sm">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('backend.layouts.master')

@section('heading')

@endsection

@section('add-new-btn')
    <button class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#add-new-file-modal">
        <i class="fa fa-plus"></i>
        Add New
    </button>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Forms
                </div>
                <div class="card-body">
                    <table id="files-table" class="table table-borderless">
                        <thead>
                        <th> File</th>
                        <th> Action</th>
                        </thead>
                        <tbody class="text-capitalize">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--    add new model --}}
    <div class="modal fade bd-example-modal-lg" id="add-new-file-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                Upload a new form
                            </div>

                            <div class="col-md-2">
                                <p id="success-msg" class="badge text-success" style="font-size: 14px"></p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="add-pdf-file">
                            <div class="form-group row">
                                <label class="col-md-3">File name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name">
                                    <label class="badge text-danger error-badge" id="name"></label>
                                </div>
                            </div>
                            <div class="form-group mt-5">
                                <div class="col-md-6 offset-5">
                                    <label class="custom-file-upload">
                                        Choose file
                                        <input type="file" class="" name="file">
                                    </label>
                                    <label class="badge text-danger error-badge" id="file"></label>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group text-right">
                            <button id="upload-btn" type="submit" class="btn btn-success btn-sm"><i class="fa fa-file-upload"></i></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('backend.layouts.master')

@section('heading')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="modal-content">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                Edit Information
                            </div>
                            <div class="col-md-2">
                                <p id="success-msg" class="badge text-success" style="font-size: 14px"></p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" method="post"
                            action="{{ route('verification.update', $data->id) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group row">
                                <label class="col-md-3">Learner Number</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="certificate_no"
                                        value="{{ $data->certificate_no }}" disabled>
                                    <label class="badge text-danger error-badge" id="certificate_no" ></label>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">First name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="f_name" value="{{ $data->f_name }}">
                                    <label class="badge text-danger error-badge" id="f_name"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Last name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="l_name" value="{{ $data->l_name }}">
                                    <label class="badge text-danger error-badge" id="l_name"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">Programme</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="course" value="{{ $data->course }}">
                                    <label class="badge text-danger error-badge" id="course"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">GPA</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="gpa" value="{{ $data->gpa }}" step=".01">
                                    <label class="badge text-danger error-badge" id="gpa"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Completion Date</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="completion_date"
                                        value="{{ $data->completion_date }}">
                                    <label class="badge text-danger error-badge" id="completion_date"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">Method of study</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="mode" value="{{ $data->mode }}">
                                    <label class="badge text-danger error-badge" id="mode"></label>
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <label class="col-md-3">Country</label>
                                <div class="col-md-6">
                                    <div class="form-control form-group pt-0">
                                        <select id="country" class="input-field" style="width: 100%" name="country"
                                            data-error="Country is required." required="required">
                                            <option value="">Select Country..</option>
                                            @foreach ($allCountries as $country)
                                                <option value="{{ $country }}"
                                                    {{ $data->country == $country ? 'selected' : '' }}> {{ $country }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <label class="badge text-danger error-badge" id="country"></label>
                                    </div> <!-- singel form -->
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">Completion Letter</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="pdf" accept="application/pdf">
                                    <label class="badge text-danger error-badge" id="pdf"></label>

                                    @if($data->pdf)
                                        <a class="btn btn-primary mt-3" href="{{ url('pdfs/'.$data->pdf) }}" class="mt-5" download> Completion Letter - Download</a>
                                    @endif

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Profile Photo</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="image" accept="image/*">
                                    <label class="badge text-danger error-badge" id="image"></label>
                                    @if($data->image)
                                        <img src="{{ url('/images/verifications/'.$data->image) }}" class="w-25 img-thumbnail mt-2" >
                                    @endif
                                </div>
                            </div>
                           
                          
                            {{-- <div class="form-group row">
                                <label class="col-md-3">Student ID</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="learner_no"
                                        value="{{ $data->learner_no }}" disabled>
                                    <label class="badge text-danger error-badge" id="learner_no"  ></label>
                                </div>
                            </div> --}}

                            {{-- <div class="form-group row">
                                <label class="col-md-3">Course</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="course">
                                    <label class="badge text-danger error-badge" id="course"></label>
                                </div>
                            </div> --}}
                    </div>
                    <div class="card-footer">
                        <div class="form-group text-right">
                            <a class="btn btn-outline-primary btn-sm" href="{{ route('admin.verification') }}">Back</a>
                            <button id="submit-btn" type="submit" class="btn btn-outline-success btn-sm">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

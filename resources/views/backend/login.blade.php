@extends('front.layouts.app')

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('f-assets/css/assets.css') }}">

	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('f-assets/css/typography.css') }}">

	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('f-assets/css/shortcodes/shortcodes.css') }}">

	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('f-assets/css/style.css') }}">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('f-assets/css/color/color-1.css') }}">
@endpush

@section('content')
    <div class="account-form">
        <div class="account-head" style="background-image:url(f-assets/images/background/bg2.jpg);">
            <a href="{{ route('home') }}"><img src="{{ asset('f-assets/images/logo-white.png') }}" alt=""></a>
        </div>
        <div class="account-form-inner">
            <div class="account-container">
                <div class="heading-bx left">
                    <h2 class="title-head">Login to your <span>Account</span></h2>
                </div>
                <form id="login-form" class="user contact-bx">
                    <div class="row placeani">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Username</label>
                                    <input name="username" type="text" required="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Password</label>
                                    <input name="password" type="password" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group form-forget">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">Remember me</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 m-b30">
                            <button name="submit" type="submit" value="Submit"
                                class="btn btn-primary btn-user">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
        });
        var $base_url = '{{ Request::url() }}';
        //    login
        $('#login-form').submit(function(e) {
            e.preventDefault();
            var btn = $("#login-form button");
            var url = $base_url + "/login";
            var data = $(this).serialize();
            var type = "post";

            btn.text("loading..");

            ajPost(url, data, type, function(r) {
                if (r.error == 0) {
                    location.href = $base_url + '/dashboard';
                } else {
                    $("#login-error").fadeIn().text(r.msg + "!");

                    setTimeout(function() {
                        $("#login-error").fadeOut();
                    }, 4000)
                }

                btn.text('Login');
            })
        })

        function ajPost(url, data, type, callback) {
            $.ajax({
                url,
                type,
                data,
                dataType: "json",
                success: function(r) {
                    callback(r);
                }
            })
        }
    </script>
@endpush

@extends('backend.layouts.master')

@section('heading')
@endsection

@section('add-new-btn')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Student Applications
                </div>
                <div class="card-body table-responsive">
                    <table id="application-table" class="table table-borderless">
                        <thead>
                        <th> Programme</th>
                        <th> Fullname</th>
                        <th> Nic / Passport</th>
                        <th> DOB</th>
                        <th> Gender</th>
                        <th> Address</th>
                        <th> Postal Code</th>
                        <th> Country</th>
                        <th> Contact</th>
                        <th> Email</th>
                        <th> Action</th>
                        </thead>
                        <tbody class="text-capitalize">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--    add new model --}}

@endsection

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table karo.application
CREATE TABLE IF NOT EXISTS `application` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `programme` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic_passport` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.application: ~27 rows (approximately)
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` (`id`, `programme`, `fname`, `nic_passport`, `gender`, `address`, `city`, `postal_code`, `country`, `contact`, `email`, `created_at`, `updated_at`, `dob`, `flag`) VALUES
	(1, 'Agro Industrial Engineering', 'Seinulabdeen Mohamed Hakeem', '962803806V', 'Male', 'No:105, New Town, Nachcahduwa, Anuradhapura.', '', '50040', 'SriLanka', '(009) 477-4423-632_', 'Hakeem96106@gmail.com', NULL, '2022-04-16 16:34:44', '10/06/1996', 1),
	(2, 'PHD in Sociology', 'Thaseem Mohammed', 'N7655688', 'Male', 'Colombo', '', '01500', 'Srilankan', '(094) 777-5818-88__', 'dr_thaseem@yahoo.com', NULL, '2022-04-16 16:34:44', '07/07/1878', 1),
	(3, 'PHD in Management', 'Nalinda Nuwan', '811621544v', 'Male', 'Colombo, Sri lanka', '', '1160', 'Sri Lanka', '(094) 077-9063-8473', 'annuwan@gmail.com', NULL, '2022-04-16 16:34:44', '10/06/1981', 1),
	(4, 'Bachelors/Masters in Business Administration', 'Hettiarachchige Indika Darshana Weerasinghe', '821550041v', 'Male', '212/18 Sobhitha Mawatha ,Nagda,Kalutara', '', '12000', 'Sri Lanka', '(940) 717-9321-17__', 'iweerasing@gmail.com', NULL, '2022-04-16 16:34:44', '06/03/1984', 1),
	(5, 'Lab Assistant', 'Mohammed Najeem', '861192067V', 'Male', 'Main street kalmunai', '', '32300', 'Sri Lanka', '(009) 470-7770-666_', 'mnajeem20@gmail.com', NULL, '2022-04-16 16:34:44', '04/28/1986', 1),
	(6, 'Civil Engineering', 'Ahamed', '8917', 'Male', '124 Al manar road', '', '323213', 'Srilanka', '(077) 676-8880-____', 'Azaamjeee@gmail.com', NULL, '2022-04-16 16:34:44', '02/02/1988', 1),
	(7, 'Agro Industrial Engineering', 'Rasika Baskaran', '915640443V', 'Fe-male', 'Kandy srilanka', '', '20000', 'Srilanka', '(947) 706-6026-9___', 'rash666@hotmail.com', NULL, '2022-04-16 16:34:44', '07/03/4', 1),
	(8, 'Bachelor of Tourism', 'Thanuda', 'n326995', 'Male', '123', '', '00700', 'Sri Lanka', '(947) 122-5554-4___', 'soorige.kumara@gmail.com', NULL, '2022-04-16 16:34:44', '03/01/1991', 1),
	(15, '123', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-15', 1),
	(16, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-22', 1),
	(17, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-21', 1),
	(18, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-15', 1),
	(19, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-22', 1),
	(20, '123', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-15', 1),
	(21, '123', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-15', 1),
	(22, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-08-04', 1),
	(23, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-14', 1),
	(24, '123', 'Soloman Vandy', '123', '12', '123,ParkStreet', '', '400081', 'India', '09820998209', 'solomanv@test.com', NULL, '2022-04-16 16:34:44', '2021-07-23', 1),
	(25, '123', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-30', 1),
	(26, 'Construction Engineer', 'one', '134', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-15', 1),
	(27, 'Physiotherapy Technician', 'Soloman Vandy', '134', '12', '123,ParkStreet', '', '400081', 'India', '09820998209', 'solomanv@test.com', NULL, '2022-04-16 16:34:44', '2021-07-20', 1),
	(28, 'Civil Engineer', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-24', 1),
	(29, 'Civil Engineer', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123asd315', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-24', 1),
	(30, 'Civil Engineer', 'one', '1123', '12', 'main street', '', '32145', 'Sri Lanka', '0981123asd315123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-24', 1),
	(31, 'Civil Engineer', 'one', '1123', '12315', 'main street', '', '32145', 'Sri Lanka', '0981123asd315123', 'one@two.com', NULL, '2022-04-16 16:34:44', '2021-07-24', 1),
	(32, '123', 'Soloman Vandy', '134', '34', 'm', '', '400081', 'India', '09820998209', 'asd@asd', NULL, '2022-04-16 16:34:44', '2022-04-03', 1),
	(33, '123', 'Soloman', '134', '12', '123,ParkStreet', '', '400081', 'India', '09820998209', 'solomanv@test.com', NULL, '2022-04-16 16:34:44', '2022-04-17', 1);
/*!40000 ALTER TABLE `application` ENABLE KEYS */;

-- Dumping structure for table karo.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table karo.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `phone` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.messages: ~4 rows (approximately)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` (`id`, `fname`, `lname`, `email`, `subject`, `message`, `date`, `flag`, `phone`) VALUES
	(7, 'Rasika', 'Baskaran', 'rash666@hotmail.com', 'Program inquiry', 'I would like to know the modules or the subjects you offer for Masters in Education', 'Tuesday 20th of April 2021 12:06:44 PM', 1, NULL),
	(26, 'Soloman Vandy', ' ', 'solomanv@test.com', 'as', 'as', 'Thursday 7th of April 2022 08:45:14 PM', 1, 9820998209),
	(27, 'rrrrrrrrrrr', ' ', 'r@r', 'as', 'qwe', 'Tuesday 12th of April 2022 01:28:15 AM', 1, 981123),
	(28, 's', ' ', 'S@s', 's', 's', 'Tuesday 12th of April 2022 01:30:12 AM', 1, 981123);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Dumping structure for table karo.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.migrations: ~15 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(3, '2020_10_18_045951_create_verification_table', 1),
	(4, '2020_10_22_144645_create_mode_column_verifications_table', 2),
	(5, '2020_10_24_113222_create_pdf_files_table', 3),
	(6, '2020_10_27_143827_create_application_table', 4),
	(7, '2020_10_28_155427_add_dob_column_to_application', 5),
	(8, '2020_10_28_193338_create_message_table', 6),
	(9, '2021_07_04_013124_add_flag_column_to_messages_table', 7),
	(10, '2021_07_04_014105_add_flag_column_to_application_table', 8),
	(13, '2021_07_04_045911_add_phone_column_to_messages_table', 9),
	(14, '2021_07_04_050646_add_academic_year_column_to_verifications_table', 9),
	(15, '2022_04_05_002654_add_gpa_column_to_verification_table', 10),
	(16, '2022_04_05_002823_add_columns_to_verifications_table', 11),
	(17, '2022_04_17_091000_gpa_column_to_varchar_verifications_table', 12),
	(18, '2022_04_17_092617_leanerno_column_to_verifications_table', 13);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table karo.pdf_files
CREATE TABLE IF NOT EXISTS `pdf_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.pdf_files: ~2 rows (approximately)
/*!40000 ALTER TABLE `pdf_files` DISABLE KEYS */;
INSERT INTO `pdf_files` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(17, 'University Profile', NULL, NULL),
	(18, 'Application Form', NULL, NULL);
/*!40000 ALTER TABLE `pdf_files` ENABLE KEYS */;

-- Dumping structure for table karo.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'user', '', NULL, '$2y$10$3FMOM1TNuz3h3nojBWnzSebMd35UXnSPhsTt1DvAh4Rg6snVQaCvm', NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table karo.verifications
CREATE TABLE IF NOT EXISTS `verifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `certificate_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completion_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `academic_year` int(11) DEFAULT NULL,
  `pdf` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `gpa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `learner_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table karo.verifications: ~38 rows (approximately)
/*!40000 ALTER TABLE `verifications` DISABLE KEYS */;
INSERT INTO `verifications` (`id`, `certificate_no`, `f_name`, `l_name`, `course`, `country`, `completion_date`, `created_at`, `updated_at`, `mode`, `academic_year`, `pdf`, `image`, `gpa`, `learner_no`) VALUES
	(15, '85875', 'S. Abdul Cader', 'Mohamed Azwath', 'BSC IN CIVIL ENGINEERING', 'Sri Lanka', '06/10/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(18, '65473', 'VARJANEE', 'KRISHNANATHAN', 'BA IN ENGLISH', 'Sri Lanka', '06/12/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(23, '65641', 'THUVAIRAJA', 'SUNDARAMOHAN', 'BA IN POLITICAL SCIENCE', 'Sri Lanka', '11/10/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(25, '65571', 'SUPPAIH', 'SATHIYADEVAN', 'BA IN ENGLISH', 'Sri Lanka', '09/12/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(26, '65662', 'SEHU ABDUL', 'MAJEEDA BEGUM', 'BA IN ENGLISH', 'Sri Lanka', '11/04/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(27, '86065', 'THINESH', 'GOWSHI AMHAYA', 'BBA & Management', 'Sri Lanka', '09/04/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(54, '65480', 'ETHIGE IMAL', 'KAVISHA SILVA', 'BTEC IN COMPUTER SCIENCE AND COMPUTER ENGINEERING', 'Sri Lanka', '2022-04-08', NULL, '2022-04-12 02:42:42', 'FULL TIME', NULL, NULL, '1649731362.png', NULL, NULL),
	(55, '81461', 'THURAIRAJAH', 'NIRTHIKAN', 'BBA', 'Sri Lanka', '06/10/2019', NULL, NULL, 'FULL TIME', NULL, NULL, NULL, NULL, NULL),
	(58, '87316', 'SIRAJUDEEN', 'MUTHIURRAHMAN', 'BSC IN NURSING', 'Sri Lanka', '07/18/2020', NULL, NULL, 'FULL TIME', NULL, NULL, NULL, NULL, NULL),
	(60, '85896', 'UTHUMANLEBBE', 'MUHAMED AMJATH', 'BSC IN COMPUTER SCIENCE & ENGINEERING', 'Sri Lanka', '07/25/2019', NULL, NULL, 'FULL TIME', NULL, NULL, NULL, NULL, NULL),
	(61, '54234', 'MOHAMMED ILLMUDEEN', 'MOHAMMED SHAMEER', 'BTEC IN COMPUTER SCIENCE AND ENGINEERING', 'Sri Lanka', '07/18/2016', NULL, NULL, 'FULL TIME', NULL, NULL, NULL, NULL, NULL),
	(64, '81496', 'THIRUSUSAI', 'BALAMURALI', 'BA SOCIAL SCIENCE', 'Sri Lanka', '06/10/2019', NULL, NULL, 'FULL TIME', NULL, NULL, NULL, NULL, NULL),
	(67, '65487', 'THILAKARATHNE WICKRAMASINGHE', 'LEO BENTLEY', 'BSc In Psychology & Counselling', 'Sri Lanka', '06/12/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(68, '85959', 'THATCHANAMOORTHY', 'MANJULA', 'BSc In Psychology', 'Sri Lanka', '06/15/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(72, '87302', 'SATHYA', 'PERINPANATHAN', 'BSc In Information Technology', 'Sri Lanka', '06/20/2020', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(73, '87428', 'SHANIKA  MARINA  DIAS', 'BANDARANAYAKA', 'Bachelor of Education In Early Childhood Education', 'Sri Lanka', '09/20/2020', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(74, '85917', 'POLHENA  MUHANDIRAMGE  GAYANI', 'NADEEKA  KUMARADASA', 'Bachelor of Education', 'Sri Lanka', '06/04/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(76, '87330', 'THANTHULAGE  ANUSHKA', 'VISHANTHA FERNANDO', 'PhD In Bio Medical Science', 'Sri Lanka', '06/20/2020', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(80, '86036', 'PARPHON', 'KINGSLEE', 'BA In English Teaching', 'Sri Lanka', '07/15/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(82, '85938', 'UDAIYAN', 'RATHAKRISHNAN', 'Bachelor of Science', 'Sri Lanka', '06/12/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(84, '65515', 'FATHIMA', 'SHABNA ILMY', 'BSc In Psychology & Counselling', 'Sri Lanka', '2022-04-09', NULL, '2022-04-12 02:26:39', 'Full Time', NULL, '1649730399.pdf', '1649730399.jpeg', NULL, NULL),
	(87, '53447', 'S.H. MOHAMMED  ASHRAFF', 'MUBARAK', 'Bachelor of Business Administration', 'Sri Lanka', '06/15/2015', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(91, '86029', 'SAJIDHA', 'ZAHEER', 'BSc In Psychology & Counselling', 'Sri Lanka', '06/15/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(94, '86015', 'SARAH', 'SHIRAZ', 'BSc In Psychology & Counselling', 'Sri Lanka', '06/15/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(98, '65585', 'WIJAYANANDAN  SOUDARYA', 'KARTHIK', 'BSC IN INFORMATION TECHNOLOGY', 'Sri Lanka', '09/12/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(101, '45214', 'THANTHALAGE ANUSHKA  VISHANTHA', 'FERNANDO', 'BSc In Bio Medical Studies', 'Sri Lanka', '07/20/2010', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(102, '87337', 'THANTHULAGE  ANUSHKA VISHANTHA', 'FERNANDO', 'PhD In Bio Medical Science', 'Sri Lanka', '06/20/2020', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(103, '81398', 'VINCENT', 'DAVID LIVINSON', 'BA In English Teaching', 'Sri Lanka', '06/20/2019', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(104, '54206', 'Dulindu Sulakna', 'Weerakoon', 'Bachelor of Business Management', 'Sri Lanka', '2022-04-20', NULL, '2022-04-17 09:19:49', 'Full Time', NULL, '1650187189.pdf', '1650187189.jpeg', '123.12', NULL),
	(106, '55225', 'THAMBIKANDU HAYATHU', 'MOHAMED', 'Bachelor of Business Administration', 'Sri Lanka', '07/25/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(108, '87344', 'SATHASIVAM', 'NISHANTHANI', 'Bachelor of Education', 'Sri Lanka', '07/20/2020', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(113, '64337', 'NITHTHIYANANTHAM', 'DISHANTHINI', 'Bachelor of Arts in English', 'Sri Lanka', '09/25/2018', NULL, NULL, 'Full Time', NULL, NULL, NULL, NULL, NULL),
	(115, '12333', 'Soloman', 'Vandy', 'HND', 'India', '08/05/2021', NULL, NULL, '123', NULL, NULL, NULL, NULL, NULL),
	(116, '111', 'one', 'Vandy', 'HND', 'Andorra', '2022-04-23', NULL, NULL, 'online', NULL, '1649360109.pdf', '1649360109.jpeg', NULL, NULL),
	(117, '123331232', 'Soloman', 'Vandy', 'DD', 'India', '2022-04-15', '2022-04-12 01:58:33', '2022-04-12 01:58:33', 'online', NULL, '1649728713.pdf', '1649728712.jpeg', NULL, NULL),
	(118, '3333999', 'Soloman', 'Vandy', 'HND Test', 'India', '2022-04-09', '2022-04-12 01:59:22', '2022-04-12 01:59:22', 'online', NULL, '1649728762.pdf', '1649728762.jpeg', NULL, NULL),
	(119, '769866766', 'ooooooooooo', 'Vandy', 'HND Test', 'India', '2022-04-15', '2022-04-12 02:00:53', '2022-04-12 02:00:53', 'online', NULL, '1649728853.pdf', '1649728851.ico', NULL, NULL),
	(120, '666', 'Test', 'Vandy', 'HND', 'India', '2022-04-14', '2022-04-12 02:43:33', '2022-04-12 02:43:33', 'online', NULL, '1649731413.pdf', '1649731413.jpeg', NULL, NULL),
	(121, '134', 'Soloman', 'Vandy', 'HND A', 'Iran (Islamic Republic of)', '2022-04-15', '2022-04-17 09:27:07', '2022-04-17 09:27:07', 'online', NULL, '1650187627.pdf', '1650187627.jpeg', '123.12', NULL),
	(122, '1343', 'Soloman', 'Vandy', 'DD', 'India', '2022-04-21', '2022-04-17 09:31:59', '2022-04-17 09:31:59', 'online', NULL, '1650187919.pdf', '1650187919.jpeg', '4.34', NULL),
	(123, '1343234', 'Soloman', 'Vandy', 'HND A', 'India', '2022-04-28', '2022-04-17 09:33:51', '2022-04-17 09:33:51', '123', NULL, '1650188031.pdf', '1650188031.jpeg', '3.4', NULL);
/*!40000 ALTER TABLE `verifications` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
